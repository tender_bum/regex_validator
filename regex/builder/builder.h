#ifndef _BUILDER_H_
#define _BUILDER_H_

#include <regex/parser/parser.h>
#include <util/ptr_view.h>
#include <util/logger/logger.h>

#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <vector>

namespace regex {

template <class Parser>
class basic_builder {
public:
    using _Self = basic_builder;
    using parser_type = Parser;
    using parser_node = typename parser_type::node;
    using value_type = typename Parser::value_type;
    using traits_type = typename Parser::traits_type;

public:
    basic_builder() = default;
    basic_builder(std::unique_ptr<parser_node> root) { // TODO
        build_impl(std::move(root));
    }
    basic_builder(parser_type psr)
        : basic_builder(psr.release_tree())
    {
    }
    basic_builder(const _Self&) = delete; // TODO?
    basic_builder(_Self&&) = default;
    _Self& operator =(const _Self&) = delete; // TODO?
    _Self& operator =(_Self&&) = default;
    ~basic_builder() = default;

    void swap(_Self& rhs) {
        // TODO
    }
    void build(std::unique_ptr<parser_node> root) {
        _Self tmp(std::move(root));
        swap(tmp);
    }
    void build(parser_type psr) {
        build(psr.release_tree());
    }
    void parse_and_build(const value_type* str) {
        build(parser_type{str});
    }
    void parse_and_build(const std::string& str) {
        parse_and_build(str.c_str());
    }

private:
    struct node1 {
        using transition_storage = std::vector<
            std::pair<util::ptr_view<parser_node>, node1*>
        >;
        bool is_final{false};
        transition_storage transitions;
    };
    struct node2 {
        using transition_storage = std::vector<
            std::pair<value_type, node2*>
        >;
        bool is_final{false};
        transition_storage transitions;
    };

    void build_impl(std::unique_ptr<parser_node> parse_root) {
        // Making FST with simple transitions
        auto init_n1 = new node1;
        {
            auto last_n1 = new node1;
            last_n1->is_final = true;
            init_n1->transitions.emplace_back(parse_root, last_n1);
            std::unordered_set<node1*> visited;
            DEBUG_LOG() << "Init: " << init_n1;
            DEBUG_LOG() << "Last: " << last_n1;
            dfs1(init_n1, visited);
        }
        DEBUG_LOG() << "--------------- dfs1 completed ---------------";

        node2* init_n2;
        {
            std::unordered_map<node1*, node2*> node_mapper;
            init_n2 = dfs1to2(init_n1, node_mapper);
            for (const auto& p : node_mapper)
                delete p.first;
        }
        DEBUG_LOG() << "--------------- dfs2 completed ---------------";

        // Getting rid of null-transitions
        // TODO

        // Making determinated FST
        // TODO
    }

    void dfs1(node1* p, std::unordered_set<node1*>& vis) {
        vis.insert(p);

        // Resulting simple transitions queue
        typename node1::transition_storage buf;

        while (!p->transitions.empty()) {
            auto tns = p->transitions.back();
            p->transitions.pop_back();

            // Null-transitions treatment
            if (tns.first == nullptr) {
                buf.emplace_back(nullptr, tns.second);
                continue;
            }

            // Connecting:
            // p -> ... -> tns.second
            std::visit([&p, &tns, &buf](auto&& v) {
                using T = std::decay_t<decltype(v)>;

                if constexpr (std::is_same_v<T, typename parser_type::union_node>) {
                    for (const auto& chd : v.childs)
                        p->transitions.emplace_back(chd, tns.second);
                } else if constexpr (std::is_same_v<T, typename parser_type::concat_node>) {
                    auto lst = p;
                    for (std::size_t i = 0; i < v.childs.size() - 1; ++i) {
                        const auto& chd = v.childs[i];
                        auto tmp = new node1;
                        lst->transitions.emplace_back(chd, tmp);
                        lst = tmp;
                    }
                    lst->transitions.emplace_back(v.childs.back(), tns.second);
                } else if constexpr (std::is_same_v<T, typename parser_type::kleene_node>) {
                    auto tmp = new node1;
                    p->transitions.emplace_back(nullptr, tmp);
                    tmp->transitions.emplace_back(v.child, tmp); // self-loop for any number of transitions
                    tmp->transitions.emplace_back(nullptr, tns.second);
                } else if constexpr (std::is_same_v<T, typename parser_type::atom_node>) {
                    buf.emplace_back(tns);
                }
            }, *tns.first);
        }
        p->transitions = std::move(buf);

        // Recursively building for descendants of p
        for (const auto& tns : p->transitions) {
            if (!vis.count(tns.second))
                dfs1(tns.second, vis);
        }

        DEBUG_LOG() << "Transitions of " << p << ":";
        for (const auto& tns : p->transitions) {
            if (tns.first == nullptr)
                DEBUG_LOG() << '<' << tns.second << ", " << "null>";
            else
                DEBUG_LOG() << '<' << tns.second << ", " << std::get<typename parser_type::atom_node>(*tns.first).value << '>';
        }
    }
    node2* dfs1to2(node1* p, std::unordered_map<node1*, node2*>& node_mapper) {
        if (node_mapper.count(p))
            return node_mapper[p];
        auto p2 = new node2;
        p2->is_final = p->is_final;
        node_mapper.emplace(p, p2);
        for (auto& tns : p->transitions)
            p2->transitions.emplace_back(
                tns.first == nullptr ? '\0' : std::get<typename parser_type::atom_node>(*tns.first).value,
                dfs1to2(tns.second, node_mapper)
            ); // '\0' for null-transition

        DEBUG_LOG() << "Transitions of " << p2 << ":";
        for (const auto& tns : p2->transitions) {
            if (tns.first == '\0')
                DEBUG_LOG() << '<' << tns.second << ", " << "null>";
            else
                DEBUG_LOG() << '<' << tns.second << ", " << tns.first << '>';
        }

        return p2;
    }


};

using builder = basic_builder<parser>;

} // regex

#endif // _BUILDER_H_
