#include <cstdio>
#include <string>

#include <regex/parser/parser.h>

using namespace std;

const int N = 1e5 + 5;

char buf[N];
regex::parser rps;

void test_expr(const char* expr) {
    cout << "Test expression: ";
    printf("%s\n", expr);
    cout << "Parsing...";
    try {
        rps.parse(expr);
    } catch (const char* mes) {
        fprintf(stderr, "%s\n", mes);
        return;
    }
    cout << "done" << endl;
    cout << "Parse tree:" << endl;
    rps.show(cout);
    cout << string(30, '-') << endl;
}

const char* to_test[] = {
    "(A|a)*z",
    "a|b|c",
    "abc",
    "(((a*)b)*c)*d",
    "(aA)(bB)|(aA)|(bB)",
    "ab|cd|ef*"
};

int main() {
    for (auto s : to_test)
        test_expr(s);
    cout << "Custom tests" << endl;
    while (true) {
        cout << "Enter expression: ";
        scanf("%s", buf);
        test_expr(buf);
    }

    return 0;
};
