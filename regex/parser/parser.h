#ifndef _PARSER_H_
#define _PARSER_H_

#include <memory>
#include <iostream>
#include <cctype>
#include <string>
#include <vector>
#include <variant>

namespace regex {

template <class CharT, class Traits = std::char_traits<CharT>>
class basic_parser {
public:
    using _Self = basic_parser;
    using value_type = CharT;
    using traits_type = Traits;

public:
    struct union_node;
    struct concat_node;
    struct kleene_node;
    struct atom_node;
    using node = std::variant<union_node, concat_node, kleene_node, atom_node>;

    struct union_node {
        std::vector<std::unique_ptr<node>> childs;
    };
    struct concat_node {
        std::vector<std::unique_ptr<node>> childs;
    };
    struct kleene_node {
        std::unique_ptr<node> child;
    };
    struct atom_node {
        value_type value;
    };

public:
    basic_parser() = default;
    basic_parser(const value_type* str)
        : _expr(str)
        , _root(union_ntl())
    {
        _expr = nullptr;
    }
    basic_parser(const std::basic_string<value_type>& str)
        : basic_parser(str.c_str())
    {
    }
    basic_parser(const _Self&) = delete; // TODO?
    basic_parser(_Self&&) = default;
    _Self& operator =(const _Self&) = delete; // TODO?
    _Self& operator =(_Self&&) = default;
    ~basic_parser() = default;

    void swap(_Self& rhs) {
        std::swap(_root, rhs._root);
    }
    void parse(const value_type* str) {
        _Self tmp(str);
        swap(tmp);
    }
    void parse(const std::basic_string<value_type>& str) {
        parse(str.c_str());
    }
    std::unique_ptr<node> release_tree() {
        return std::move(_root);
    }
    void show(std::basic_ostream<value_type, traits_type>& fout) const {
        show(_root, fout);
    }
    static void show(
        const std::unique_ptr<node>& root,
        std::basic_ostream<value_type, traits_type>& fout)
    {
        show_impl(root, fout, 0);
    }

private:
    static bool is_regend(value_type c) {
        return c == '|' || c == ')' || c == 0;
    }
    void match(value_type c) {
        if (*_expr != c)
            throw "Syntax error";
        ++_expr;
    };
    template <class F>
    void match(F f) {
        if (!f(*_expr))
            throw "Syntax error";
        ++_expr;
    }
    static void show_impl(
        const std::unique_ptr<node>& u,
        std::basic_ostream<value_type, traits_type>& fout,
        int d)
    {
        std::visit([&fout, d](auto&& n) {
            using T = std::decay_t<decltype(n)>;
            fout << std::string(d, '\t');
            if constexpr (std::is_same_v<T, union_node>) {
                fout << '|' << std::endl;
                for (const auto& p : n.childs)
                    show_impl(p, fout, d + 1);
            } else if constexpr (std::is_same_v<T, concat_node>) {
                fout << '+' << std::endl;
                for (const auto& p : n.childs)
                    show_impl(p, fout, d + 1);
            } else if constexpr (std::is_same_v<T, kleene_node>) {
                fout << '*' << std::endl;
                show_impl(n.child, fout, d + 1);
            } else if constexpr (std::is_same_v<T, atom_node>) {
                fout << n.value << std::endl;
            }
        }, *u);
    }

private:
    std::unique_ptr<node> union_ntl() {
        auto res = concat_ntl();
        if (*_expr == '|') {
            auto tmp = std::make_unique<node>(union_node{});
            std::get<union_node>(*tmp).childs.push_back(std::move(res));
            res = std::move(tmp);
        }
        while (*_expr == '|') {
            match('|');
            std::get<union_node>(*res).childs.push_back(concat_ntl());
        }
        return res;
    }

    std::unique_ptr<node> concat_ntl() {
        auto res = kleene_ntl();
        if (!is_regend(*_expr)) {
            auto tmp = std::make_unique<node>(concat_node{});
            std::get<concat_node>(*tmp).childs.push_back(std::move(res));
            res = std::move(tmp);
        }
        while (!is_regend(*_expr))
            std::get<concat_node>(*res).childs.push_back(kleene_ntl());
        return res;
    }

    std::unique_ptr<node> kleene_ntl() {
        auto res = atom_ntl();
        if (*_expr == '*') {
            match('*');
            auto tmp = std::make_unique<node>(kleene_node{});
            std::get<kleene_node>(*tmp).child = std::move(res);
            res = std::move(tmp);
        }
        return res;
    }

    std::unique_ptr<node> atom_ntl() {
        if (*_expr == '(') {
            match('(');
            auto res = union_ntl();
            match(')');
            return res;
        }
        auto res = std::make_unique<node>(atom_node{*_expr});
        match([](value_type c){return std::isalpha(c);});
        return res;
    }

private:
    const value_type* _expr{nullptr};
    std::unique_ptr<node> _root;

};

using parser = basic_parser<char>;

} // regex

#endif // _PARSER_H_
