#include <util/logger/logger.h>
#include <iostream>

int main() {
    LOG() << "INFO test" << "Second sentance";
    WARNING_LOG() << "WARNING test";
    ERROR_LOG() << "ERROR test with brackets";

    return 0;
}
