#ifndef _PTR_VIEW_H_
#define _PTR_VIEW_H_

#include <memory>

namespace util {

template <class T>
class ptr_view {
public:
    using pointer = std::add_const_t<T>*;
    using element_type = T;

public:
    constexpr ptr_view() noexcept = default;
    constexpr ptr_view(std::nullptr_t)
        : _ptr(nullptr)
    {
    }
    ptr_view(pointer p) noexcept
        : _ptr(p)
    {
    }
    ptr_view(const std::unique_ptr<T>& p) noexcept
        : _ptr(p.get())
    {
    }
    ptr_view(const std::shared_ptr<T>& p) noexcept
        : _ptr(p.get())
    {
    }
    ptr_view(const ptr_view&) = default;
    ptr_view(ptr_view&& p)
        : _ptr(p._ptr)
    {
        p._ptr = nullptr;
    }
    ptr_view& operator =(const ptr_view&) = default;
    ptr_view& operator =(ptr_view&& p) {
        ptr_view tmp(std::move(p));
        swap(tmp);
    }
    void swap(ptr_view& rhs) noexcept {
        std::swap(_ptr, rhs._ptr);
    }
    void reset(pointer p = nullptr) noexcept {
        _ptr = p;
    }
    pointer release() noexcept {
        auto tmp = _ptr;
        _ptr = nullptr;
        return tmp;
    }
    pointer get() const noexcept {
        return _ptr;
    }
    pointer operator ->() const noexcept {
        return _ptr;
    }
    std::add_lvalue_reference_t<std::add_const_t<T>> operator *() const {
        return *_ptr;
    }
    explicit operator bool() const noexcept {
        return _ptr;
    }

private:
    pointer _ptr{nullptr};
    

};

template <class T1, class T2>
bool operator ==(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() == (void*)rhs.get();
}
template <class T1, class T2>
bool operator !=(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() != (void*)rhs.get();
}
template <class T1, class T2>
bool operator <(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() < (void*)rhs.get();
}
template <class T1, class T2>
bool operator <=(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() <= (void*)rhs.get();
}
template <class T1, class T2>
bool operator >(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() > (void*)rhs.get();
}
template <class T1, class T2>
bool operator >=(const ptr_view<T1>& lhs, const ptr_view<T2>& rhs) {
    return (void*)lhs.get() >= (void*)rhs.get();
}
template <class T>
bool operator ==(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() == (void*)nullptr;
}
template <class T>
bool operator !=(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() != (void*)nullptr;
}
template <class T>
bool operator <(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() < (void*)nullptr;
}
template <class T>
bool operator <=(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() <= (void*)nullptr;
}
template <class T>
bool operator >(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() > (void*)nullptr;
}
template <class T>
bool operator >=(const ptr_view<T>& lhs, std::nullptr_t) {
    return (void*)lhs.get() >= (void*)nullptr;
}
template <class T>
bool operator ==(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr == (void*)rhs.get();
}
template <class T>
bool operator !=(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr != (void*)rhs.get();
}
template <class T>
bool operator <(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr < (void*)rhs.get();
}
template <class T>
bool operator <=(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr <= (void*)rhs.get();
}
template <class T>
bool operator >(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr > (void*)rhs.get();
}
template <class T>
bool operator >=(std::nullptr_t, const ptr_view<T>& rhs) {
    return (void*)nullptr >= (void*)rhs.get();
}

} // util

#endif // _PTR_VIEW_H_
